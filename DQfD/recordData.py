import gym
#from gym.utils.play import play

import gym
import pygame
import matplotlib
import argparse
from gym import logger
from collections import deque
from pygame.locals import VIDEORESIZE
import numpy as np
import cv2
import csv
import datetime

FRAME_WIDTH = 84 # 105
FRAME_HEIGHT = 84 # 80
INPUT_SHAPE = (FRAME_WIDTH, FRAME_HEIGHT)
lastFourFrames = deque([], maxlen=4)
file_name = str(datetime.datetime.now())

def preprocess_observations(observation):
    assert observation.ndim == 3  # (height, width, channel)
    rgb_frame = observation
    img = cv2.cvtColor(rgb_frame, cv2.COLOR_RGB2GRAY)
    img = cv2.resize(img, (84, 110), interpolation=cv2.INTER_NEAREST)
    # Crop 84x84 square
    img = img[18:18 + 84, :]
    assert img.shape == INPUT_SHAPE

    return img.astype('uint8')

def add_to_the_last_four_frames(state):
    lastFourFrames.append(preprocess_observations(state))

def convert_last_four_frames():
    return np.array(lastFourFrames)

# Keeps a recond of everything seen so far.
def remember(state, action, reward, next_state, done):
    data = [state, action, reward, next_state, done]
    with open("./{}.csv".format(file_name), 'a') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerow(data)

    writeFile.close()

def display_arr(screen, arr, video_size, transpose):
    arr_min, arr_max = arr.min(), arr.max()
    arr = 255.0 * (arr - arr_min) / (arr_max - arr_min)
    pyg_img = pygame.surfarray.make_surface(arr.swapaxes(0, 1) if transpose else arr)
    pyg_img = pygame.transform.scale(pyg_img, video_size)
    screen.blit(pyg_img, (0,0))

def play(env,
         transpose=True,
         fps=30,
         zoom=None,
         callback=None,
         keys_to_action=None):
    """Allows one to play the game using keyboard.

    Arguments
    ---------
    env: gym.Env
        Environment to use for playing.
    transpose: bool
        If True the output of observation is transposed.
        Defaults to true.
    fps: int
        Maximum number of steps of the environment to execute every second.
        Defaults to 30.
    zoom: float
        Make screen edge this many times bigger
    callback: lambda or None
        Callback if a callback is provided it will be executed after
        every step. It takes the following input:
            obs_t: observation before performing action
            obs_tp1: observation after performing action
            action: action that was executed
            rew: reward that was received
            done: whether the environment is done or not
            info: debug info
    keys_to_action: dict: tuple(int) -> int or None
        Mapping from keys pressed to action performed.
        For example if pressed 'w' and space at the same time is supposed
        to trigger action number 2 then key_to_action dict would look like this:

            {
                # ...
                sorted(ord('w'), ord(' ')) -> 2
                # ...
            }
        If None, default key_to_action mapping for that env is used, if provided.
    """
    env.reset()
    rendered=env.render(mode='rgb_array')

    if keys_to_action is None:
        if hasattr(env, 'get_keys_to_action'):
            keys_to_action = env.get_keys_to_action()
        elif hasattr(env.unwrapped, 'get_keys_to_action'):
            keys_to_action = env.unwrapped.get_keys_to_action()
        else:
            assert False, env.spec.id + " does not have explicit key to action mapping, " + \
                          "please specify one manually"
    relevant_keys = set(sum(map(list, keys_to_action.keys()),[]))

    video_size=[rendered.shape[1],rendered.shape[0]]
    if zoom is not None:
        video_size = int(video_size[0] * zoom), int(video_size[1] * zoom)

    pressed_keys = []
    running = True
    env_done = True

    screen = pygame.display.set_mode(video_size)
    clock = pygame.time.Clock()


    while running:
        if env_done:
            env_done = False
            obs = env.reset()
        else:
            action = keys_to_action.get(tuple(sorted(pressed_keys)), 0)
            prev_obs = obs
            ######################1
            if len(lastFourFrames) == 0:
                add_to_the_last_four_frames(obs)
            ######################1
            obs, rew, env_done, info = env.step(action)
            ######################2
            add_to_the_last_four_frames(obs)
            obs_processed = convert_last_four_frames()
            if(len(lastFourFrames) == 4 and prev_obs_processed.shape[0] == 4):
                remember(prev_obs_processed, action, rew, obs_processed, env_done)
            prev_obs_processed = obs_processed
            ######################2

        if obs is not None:
            rendered=env.render(mode='rgb_array')
            display_arr(screen, rendered, transpose=transpose, video_size=video_size)

        # process pygame events
        for event in pygame.event.get():
            # test events, set key states
            if event.type == pygame.KEYDOWN:
                if event.key in relevant_keys:
                    pressed_keys.append(event.key)
                elif event.key == 27:
                    running = False
            elif event.type == pygame.KEYUP:
                if event.key in relevant_keys:
                    pressed_keys.remove(event.key)
            elif event.type == pygame.QUIT:
                running = False
            elif event.type == VIDEORESIZE:
                video_size = event.size
                screen = pygame.display.set_mode(video_size)
                print(video_size)

        pygame.display.flip()
        clock.tick(fps)
    pygame.quit()

env = gym.make("MontezumaRevengeNoFrameskip-v4")
play(env, zoom=4, fps=60)  # reduce fps to make the controls easier
