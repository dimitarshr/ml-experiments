import gym
import numpy as np
import random
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
import matplotlib.pyplot as plt

ENVIRONMENT_NAME = 'LunarLander-v2'
MEMORY_SIZE = 2**16
BATCH_SIZE = 2**5
GAMMA = 0.99 # discount rate
EPSILON = 1.0  # exploration rate
EPSILON_MIN = 0.0
EPSILON_DECAY = 0.998
LEARNING_RATE = 0.0001
SUCCESSFUL_EPISODES_THRESHOLD = 5
SOLVED_SCORE = 100

# Deep Q-learning Agent
class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=MEMORY_SIZE)
        self.gamma = GAMMA
        self.epsilon = EPSILON
        self.epsilon_min = EPSILON_MIN
        self.epsilon_decay = EPSILON_DECAY
        self.learning_rate = LEARNING_RATE
        self.model = self._build_model()
    
    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        model.add(Dense(128, input_dim=self.state_size, activation='relu'))
        model.add(Dense(64, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mean_squared_error',
                      optimizer=Adam(lr=self.learning_rate))
        return model
    
    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))
    
    def act(self, state, training):
        if training and np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action
    
    def replay(self, batch_size):
    	if len(self.memory) <= batch_size:
            return
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
              target = reward + self.gamma * \
                       np.amax(self.model.predict(next_state)[0])
            target_f = self.model.predict(state)
            # update the Q value
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def train(self, environment):
        self.run(environment=environment, training=True)

    # numberOfEpisodes is -1 by default because 
    # it is only used when testing the agent
    def run(self, environment, numberOfEpisodes=-1, training=False):
        #The x and y are used to draw the line graph
        xAxisValues = []
        yAxisValues = []

        if training:
            plt.subplot(2, 1, 1)
            plt.title(ENVIRONMENT_NAME + " training")
            plt.xlabel('episode')
            plt.ylabel('score (number of timesteps)')
        else:
            plt.subplot(2, 1, 2)
            plt.title(ENVIRONMENT_NAME + " testing")
            plt.xlabel('episode')
            plt.ylabel('score (number of timesteps)')
        plt.tight_layout()

        episodeCounter = 0
        running = True
        counterSuccessfulEpisodes = 0
        totalReward = 0
        while running:
            score = 0
            episodeCounter += 1
            # reset state in the beginning of each game
            state = environment.reset()
            state = np.reshape(state, [1, self.state_size])
            # time_t represents each frame of the game
            # Our goal is to keep the pole upright as long as possible until score of ENVIRONMENT_MAX_EPISODES
            # the more time_t the more score
            flying = True
            while flying:
                # Render if we are not training
                if not training:
                    env.render()
                # Decide action
                action = self.act(state, training)
                # Advance the game to the next frame based on the action.
                # There are three type of rewards
                #       +1 for every frame the pole survived
                #       +10 if it reaches ENVIRONMENT_MAX_EPISODES-10
                #       0 if it fails
                next_state, reward, done, _ = environment.step(action)
                score += reward
                if done:
                    if score >= SOLVED_SCORE-10:
                        counterSuccessfulEpisodes += 1
                    else:
                        counterSuccessfulEpisodes = 0
                next_state = np.reshape(next_state, [1, self.state_size])
                # Remember the previous state, action, reward, and done
                if training:
                    self.remember(state, action, reward, next_state, done)
                # make next_state the new current state for the next frame.
                state = next_state
                # done becomes True when the game ends
                if done:
                    # print the score and break out of the loop
                    if training:
                        print("Counter successful consequtive episodes:{}, episode: {}, score: {}, epsilon: {}"
                          .format(counterSuccessfulEpisodes, episodeCounter, score, self.epsilon))
                    else:
                        totalReward += score                        
                        print("Counter successful consequtive episodes:{}, episode: {}/{}, score: {}"
                          .format(counterSuccessfulEpisodes, episodeCounter, numberOfEpisodes, score))

                    flying = False
                    break

            # train the agent with the experience of the episode
            if training:
                self.replay(BATCH_SIZE)

            if training and episodeCounter == 1000:
                running = False

            if not training and episodeCounter == numberOfEpisodes:
                print("MEMORY_SIZE:{} \nBATCH_SIZE: {} \nGAMMA: {} \nEPSILON: {} \nEPSILON_MIN: {} \nEPSILON_DECAY: {} \nLEARNING_RATE: {} \nSUCCESSFUL_EPISODES_THRESHOLD: {} \nAverage score: {}"
                          .format(MEMORY_SIZE, BATCH_SIZE, GAMMA, EPSILON, EPSILON_MIN, EPSILON_DECAY, LEARNING_RATE, SUCCESSFUL_EPISODES_THRESHOLD, totalReward/episodeCounter))

            # plot the line graph
            if not training:
                xAxisValues.append(episodeCounter)
                yAxisValues.append(score)
                plt.plot(xAxisValues, yAxisValues, c="red")
                # plt.draw() # to be vialised in read time
                # plt.pause(0.005)
            else:
                xAxisValues.append(episodeCounter)
                yAxisValues.append(score)
                plt.plot(xAxisValues, yAxisValues, c="blue")
            
            # check if the running should be stopped
            if training and counterSuccessfulEpisodes >= SUCCESSFUL_EPISODES_THRESHOLD:
                running = False
            elif not training and episodeCounter == numberOfEpisodes:
                running = False



if __name__ == "__main__":
    # initialise gym environment and the agent
    env = gym.make(ENVIRONMENT_NAME)
    agent = DQNAgent(env.observation_space.shape[0], env.action_space.n)
    # train the agent until it overall scores converge
    agent.train(env)

    # test the trained model for 100 episodes
    testEpisodes = 100
    agent.run(environment=env, numberOfEpisodes=testEpisodes)
    plt.savefig('myfig.png', dpi=1000)
