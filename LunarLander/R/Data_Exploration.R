# TO-DO
library(ggplot2)

# Load the dataset

LunarLander <- read.csv("/home/dimitar/Desktop/LunarLanderBoxplotData.csv")
CartPole <- read.csv("/home/dimitar/Desktop/CartPoleBoxplotData.csv")
Acrobot <- read.csv("/home/dimitar/Desktop/AcrobotBoxplotData.csv")

means <- aggregate(Score ~ Trial, LunarLander, median)
ggplot(LunarLander, aes(x=Trial, y = Score, fill = Trial)) + geom_boxplot() + labs(title="LunarLander", x="Trial", y="Score")  + scale_fill_manual(values=c("#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00")) + theme_bw() + guides(fill = FALSE) + theme(plot.title=element_text(size=20, face="bold"), axis.text.x=element_text(size=10), axis.text.y=element_text(size=10), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20)) + geom_text(data = means, aes(x = Trial, label = round(Score,2), y = round(Score,2), hjust = 0.5, vjust = -0.3))

means <- aggregate(Score ~ Trial, CartPole, median)
ggplot(CartPole, aes(x=Trial, y = Score, fill = Trial)) + geom_boxplot() + labs(title="CartPole", x="Trial", y="Score")  + scale_fill_manual(values=c("#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00")) + theme_bw() + guides(fill = FALSE) + theme(plot.title=element_text(size=20, face="bold"), axis.text.x=element_text(size=10), axis.text.y=element_text(size=10), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20)) + geom_text(data = means, aes(x = Trial, label = round(Score,2), y = round(Score,2), hjust = 0.5, vjust = -0.3))

means <- aggregate(Score ~ Trial, Acrobot, median)
ggplot(Acrobot, aes(x=Trial, y = Score, fill = Trial)) + geom_boxplot() + labs(title="Acrobot", x="Trial", y="Score")  + scale_fill_manual(values=c("#e41a1c", "#377eb8", "#4daf4a", "#984ea3", "#ff7f00")) + theme_bw() + guides(fill = FALSE) + theme(plot.title=element_text(size=20, face="bold"), axis.text.x=element_text(size=10), axis.text.y=element_text(size=10), axis.title.x=element_text(size=20),axis.title.y=element_text(size=20)) + geom_text(data = means, aes(x = Trial, label = round(Score,2), y = round(Score,2), hjust = 0.5, vjust = -0.3))
