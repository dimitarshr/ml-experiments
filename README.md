# ML Experiments

The different folders refer to the different environments that are part of OpenAI gym:

* https://gym.openai.com/envs/CartPole-v0/
* https://gym.openai.com/envs/LunarLander-v2/
* https://gym.openai.com/envs/Pong-v0/

