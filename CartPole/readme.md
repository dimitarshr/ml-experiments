The images show the results (in the form of line graphs) of solving CartPole-v0. The environment is considered for solved if a score of 199 is achieved in 100 consequtive episodes.

* The first set of results (threshold_10_\*.svg) include runs in which the learning process was stopped after 10 consequtive episodes in which the score was equal to 199.

* The second set of results (threshold_30_\*.svg) include runs in which the learning process was stopped after 30 consequtive episodes in which the score was equal to 199.

The results from the threshold being 10 are a bit inconsistent. Sometimes, the agent was able to solve it and sometimes not. Please refer to the image below and the rest of the images above.
![Threshold = 10](https://bitbucket.org/dimitarshr/ml-experiments/raw/ebf4c01adb772ab413bca5ab8b47920e4fb795db/CartPole/threshold_10_results_6.png)

**On contrast, the results of the experiments with threshold of 30 were more stable. Please refer to the image below and the rest of the images above.**

![Threshold = 30](https://bitbucket.org/dimitarshr/ml-experiments/raw/ebf4c01adb772ab413bca5ab8b47920e4fb795db/CartPole/threshold_30_results_4.png)