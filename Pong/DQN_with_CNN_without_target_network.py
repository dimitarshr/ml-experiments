import sys
import gym
from gym import wrappers

from keras.models import Sequential
from keras.layers import Dense, Flatten, Lambda, Activation, Flatten, Convolution2D, Permute
from keras.optimizers import Adam
from keras.models import load_model
from keras.models import clone_model

import numpy as np
import random
import tensorflow as tf
import datetime
import csv
import time
import cv2
import os
import argparse
from collections import deque
import time

parser = argparse.ArgumentParser()
parser.add_argument('--training_seed', default=0, type=int)
parser.add_argument('--testing_seed', default=1, type=int)
args = parser.parse_args()

ENVIRONMENT_NAME = "SeaquestDeterministic-v0" #v0 - sticky actions, v4 - no sticky actions
MEMORY_MAX_SIZE = 400000
MEMORY_MIN_SIZE = 50000
BATCH_SIZE = 2**5
GAMMA = 0.99 # discount rate
EPSILON = 1.0  # exploration rate
EPSILON_MIN = 0.1
EPSILON_DELTA = 0.0000009
LEARNING_RATE = 0.0001

NUM_FRAMES = 4
FRAME_WIDTH = 84 # 105
FRAME_HEIGHT = 84 # 80
INPUT_SHAPE = (FRAME_WIDTH, FRAME_HEIGHT)

# TRAINED_MODEL = "/home/dimitar/Desktop/ml-experiments/LunarLander/models/2019-02-02 12:59:15.239834.h5"
TRAINED_MODEL = ""
ITERATIONS = 1500000
REFRESH_TARGET_MODEL = 10000
TRAIN_EVERY_N_STEPS = 1
TOTAL_TEST_EPISODES = 100

FILE_OUTPUT = True
SEED = args.training_seed
TESTING_SEED = args.testing_seed

ARGUMENTS = {
             "VERSION": sys.argv[0],
             "ENVIRONMENT_NAME": ENVIRONMENT_NAME,
             "MEMORY_MAX_SIZE": MEMORY_MAX_SIZE,
             "MEMORY_MIN_SIZE": MEMORY_MIN_SIZE,
             "BATCH_SIZE": BATCH_SIZE,
             "GAMMA": GAMMA,
             "EPSILON": EPSILON,
             "EPSILON_MIN": EPSILON_MIN,
             "EPSILON_DELTA": EPSILON_DELTA,
             "LEARNING_RATE": LEARNING_RATE,
             "NUM_FRAMES": NUM_FRAMES,
             "FRAME_WIDTH": FRAME_WIDTH,
             "FRAME_HEIGHT": FRAME_HEIGHT,
             "INPUT_SHAPE": INPUT_SHAPE,
             "ITERATIONS": ITERATIONS,
             "REFRESH_TARGET_MODEL": REFRESH_TARGET_MODEL,
             "TRAIN_EVERY_N_STEPS": TRAIN_EVERY_N_STEPS,
             "TOTAL_TEST_EPISODES": TOTAL_TEST_EPISODES,
             "FILE_OUTPUT": FILE_OUTPUT,
             "SEED": SEED,
             "TESTING_SEED": TESTING_SEED
            }

def preprocess_observations(observation):
    assert observation.ndim == 3  # (height, width, channel)
    rgb_frame = observation
    img = cv2.cvtColor(rgb_frame, cv2.COLOR_RGB2GRAY)
    img = cv2.resize(img, (84, 110), interpolation=cv2.INTER_NEAREST)
    # Crop 84x84 square
    img = img[18:18 + 84, :]
    assert img.shape == INPUT_SHAPE

    return img.astype('uint8')

def clip_reward(reward):
    return np.sign(reward)

# Deep Q-learning Agent
class DQNAgent:
    def __init__(self, action_space, environment):
        self.file_output = FILE_OUTPUT
        self.action_space = action_space
        self.gamma = GAMMA
        self.epsilon = EPSILON
        self.epsilon_min = EPSILON_MIN
        self.epsilon_delta = EPSILON_DELTA
        self.learning_rate = LEARNING_RATE
        self.train_every_n_steps = TRAIN_EVERY_N_STEPS
        self.model = self._build_model()
        self.targetModel = clone_model(self.model)
        self.targetModel.set_weights(self.model.get_weights())
        self.environment = environment
        self.memory = deque([], maxlen=MEMORY_MAX_SIZE)
        self.running_rewards = deque([], maxlen=100)
        self.xAxisValues = []
        self.yAxisValues = []
        self.isPlotInitialised = False
        self.memoryInfo = True
        self.minMemoryInfo = False
        self.dataToCSV = []
        # Last few frames are used as input to the NN as a representation of motion.
        self.lastFourFrames = deque([], maxlen=4)


    def _build_model(self):
        model = Sequential()
        input_shape = (NUM_FRAMES,) + INPUT_SHAPE
        model.add(Permute((2, 3, 1), input_shape=input_shape))
        model.add(Lambda(lambda x: x / 255.0))
        model.add(Convolution2D(32, (8, 8), strides=(4, 4)))
        model.add(Activation('relu'))
        model.add(Convolution2D(64, (4, 4), strides=(2, 2)))
        model.add(Activation('relu'))
        model.add(Convolution2D(64, (3, 3), strides=(1, 1)))
        model.add(Activation('relu'))
        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(Dense(self.action_space))
        model.add(Activation('linear'))

        optimizer = Adam(lr = LEARNING_RATE)
        model.compile(optimizer, loss='mean_squared_error')

        model.summary()

        return model


    """Converts the list of NUM_FRAMES images into one training sample"""
    def convert_last_four_frames(self):
        return np.array(self.lastFourFrames)

    def add_to_the_last_four_frames(self, state):
        self.lastFourFrames.append(preprocess_observations(state))

    # Keeps a recond of everything seen so far.
    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    # Chooses an action to be perfomed.
    def act(self, state, training):
        if np.random.rand() <= self.epsilon and training:
            action = random.randrange(self.action_space)
        else:
            tmp = self.model.predict_on_batch(state.reshape((1, NUM_FRAMES, FRAME_WIDTH, FRAME_HEIGHT)))
            action = np.argmax(tmp)

        return action

    def replay(self):
        minibatch = random.sample(self.memory, BATCH_SIZE)
        S = np.array([e[0] for e in minibatch])     # takes the state values
        A = np.array([e[1] for e in minibatch])     # takes the action values
        R = np.array([e[2] for e in minibatch])     # takes the reward vales
        T = np.array([e[3] for e in minibatch])     # takes the next_state values
        D = np.array([e[4] for e in minibatch])     # takes the done values

        q = self.model.predict_on_batch(S)
        q_t = self.model.predict_on_batch(T)

        # print("old", q[0])
        # print("q_t", q_t[0])
        # print("D", D[0], float(D[0] != False))

        for i in range(len(minibatch)):
            q[i][A[i]] = R[i] + self.gamma * np.max(q_t[i]) * float(D[i] == False)

        # print("new", q[0])

        self.model.fit(S, q, batch_size=BATCH_SIZE, epochs=1, verbose=0)
        return self.model.history.history['loss'][0]

        # self.model.train_on_batch(S, q)
        # return 0

    def train(self):
        totalFrames = 0

        cumFrames = 0
        cumReward = 0
        cumLoss = 0

        episodeRewards = []
        episodeReward = 0

        fileName = self.createCSVfile(training = True)

        start10KFrames = time.time()

        while totalFrames < ITERATIONS:

            episodeRewards.append(episodeReward)
            episodeReward = 0

            state = self.environment.reset()
            self.lastFourFrames.clear()
            self.add_to_the_last_four_frames(state)
            ###################################
            for index in range (NUM_FRAMES-1):
                action = random.randrange(self.action_space)
                state,_,_,_ = self.environment.step(action)
                self.add_to_the_last_four_frames(state)
            ###################################
            processed_state = self.convert_last_four_frames()
            done = False

            while not done:
                # each iterations is a single frame
                cumFrames += 1
                totalFrames += 1

                action = self.act(processed_state, training = True)
                next_state, reward, done, _ = self.environment.step(action)
                #reward = clip_reward(reward)
                self.add_to_the_last_four_frames(next_state)
                processed_next_state = self.convert_last_four_frames() #preprocess_observations(next_state)
                cumReward += reward
                episodeReward += reward

                self.remember(processed_state, action, reward, processed_next_state, done)
                processed_state = processed_next_state

                if len(self.memory) > MEMORY_MIN_SIZE:
                    if totalFrames % self.train_every_n_steps == 0:
                        cumLoss += self.replay()
                    self.reduce_epsilone()

                if totalFrames % REFRESH_TARGET_MODEL == 0:

                    if len(self.memory) > MEMORY_MIN_SIZE:
                        self.targetModel.set_weights(self.model.get_weights())

                    end10KFrames = time.time()

                    print('All frames: {}, Reward: {:.2f} [{}, {}], # Episodes {}, Epsilon: {:f}; Avg. loss: {:f} Memory size: {}; Time: {:.2f}s' \
                    .format(totalFrames,
                            cumReward,
                            np.min(episodeRewards),
                            np.max(episodeRewards),
                            len(episodeRewards),
                            self.epsilon,
                            cumLoss/float(cumFrames),
                            len(self.memory),
                            end10KFrames-start10KFrames))

                    self.writeToCSV([totalFrames,
                                     [cumReward,
                                     (np.min(episodeRewards),
                                     np.max(episodeRewards))],
                                     len(episodeRewards),
                                     self.epsilon,
                                     cumLoss/float(cumFrames),
                                     len(self.memory),
                                     end10KFrames-start10KFrames],
                                    fileName)

                    cumFrames = 0
                    cumLoss = 0
                    cumReward = 0

                    episodeRewards = []

                    start10KFrames = time.time()
        self.save_model()

    def test(self):
        totalFrames = 0
        episodeReward = 0
        currentTestEpisodes = 0

        fileName = self.createCSVfile(training = False)
        self.recordEnv()

        while currentTestEpisodes < TOTAL_TEST_EPISODES:
            startEpisode = time.time()
            episodeReward = 0
            totalFrames = 0

            state = self.environment.reset()
            self.lastFourFrames.clear()
            self.add_to_the_last_four_frames(state)
            ###################################
            for index in range (random.randrange(100) + NUM_FRAMES-1):
                action = 0
                state,_,_,_ = self.environment.step(action)
                self.add_to_the_last_four_frames(state)
            ###################################
            processed_state = self.convert_last_four_frames()
            done = False

            while not done:
                # each iterations is a single frame
                totalFrames += 1

                action = self.act(processed_state, training = False)
                next_state, reward, done, _ = self.environment.step(action)
                #reward = clip_reward(reward)
                self.add_to_the_last_four_frames(next_state)
                processed_next_state = self.convert_last_four_frames() #preprocess_observations(next_state)
                episodeReward += reward

                processed_state = processed_next_state

            endEpisode = time.time()

            print('Episode #{}, Frames: {}, Reward: {:.2f}, Time: {:.2f}s' \
            .format(currentTestEpisodes,
                    totalFrames,
                    episodeReward,
                    endEpisode - startEpisode))

            self.writeToCSV([currentTestEpisodes,
                             totalFrames,
                             episodeReward,
                             self.epsilon,
                             endEpisode - startEpisode],
                            fileName)

            currentTestEpisodes += 1

    def reduce_epsilone(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon -= self.epsilon_delta

    def save_model(self):
        if not os.path.exists("./models"):
            os.mkdir("./models")

        self.model.save("./models/{}-{}.h5".format(str(ENVIRONMENT_NAME), str(datetime.datetime.now())))


    def createCSVfile(self, training):
        if self.file_output:
            if not os.path.exists("./CSVdata"):
                os.mkdir("./CSVdata")

            if training:
                fileName = './CSVdata/{}-{}Training.csv'.format(str(ENVIRONMENT_NAME), str(datetime.datetime.now()))
                self.writeToCSV(["All frames", "Reward", "Episodes", "Epsilon", "Avg. loss", "Memory size", "Time"], fileName)
            else:
                fileName = './CSVdata/{}-{}Testing.csv'.format(str(ENVIRONMENT_NAME), str(datetime.datetime.now()))
                self.writeToCSV(["Episode #", "Frames", "Reward", "Epsilon", "Time"], fileName)

                argFileName = './CSVdata/{}-{}Arguments.csv'.format(str(ENVIRONMENT_NAME), str(datetime.datetime.now()))
                with open(argFileName, 'a') as writeFile:
                    writer = csv.writer(writeFile)
                    for key, value in ARGUMENTS.items():
                            writer.writerow([key, value])
                writeFile.close()

            return fileName

    def writeToCSV(self, data, fileName):
        if self.file_output:
            with open(fileName, 'a') as writeFile:
                writer = csv.writer(writeFile)
                writer.writerow(data)

            writeFile.close()

    def recordEnv(self):
        if not os.path.exists("./videos"):
            os.mkdir("./videos")

        self.environment = wrappers.Monitor(self.environment,
                                            './videos/{}-{}'.format(str(ENVIRONMENT_NAME), str(datetime.datetime.now())),
                                            force=True,
                                            video_callable=lambda episode_id: episode_id%10==0)


if __name__ == "__main__":
    # initialise gym environment and the agent
    env = gym.make(ENVIRONMENT_NAME)

    env.seed(SEED)
    random.seed(SEED)
    np.random.seed(SEED)
    tf.set_random_seed(SEED)

    agent = DQNAgent(env.action_space.n, env)

    print("\tNN created!")

    if TRAINED_MODEL == "":
        # train the agent until it overall scores converge
        agent.train()
    else:
        agent.model = load_model(TRAINED_MODEL)

    print("\t\t Starting testing!")

    env.seed(TESTING_SEED)
    random.seed(TESTING_SEED)
    np.random.seed(TESTING_SEED)
    tf.set_random_seed(TESTING_SEED)

    # test the trained model
    agent.test()
